public class Potenciacion implements Operacion {
    @Override
    public int operar(int a, int b) {
        int r = 1;
        while (b > 0) {
            if ((b & 1) != 0)
                r *= a;
            b >>= 1;
            a *= a;
        }
        return r;
    }
}
