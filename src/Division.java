public class Division implements Operacion {
    @Override
    public int operar(int a, int b) {
        return a / b;
    }
}
