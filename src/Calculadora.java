import java.util.HashMap;
import java.util.Map;


public class Calculadora {

	Map<String, Operacion> operaciones;

	public Calculadora() {
		operaciones = new HashMap<String, Operacion>();
	}

	public void registrar(String operador, Operacion operacion) {
		operaciones.put(operador, operacion);
	}

	public int ejecutar(int a, int b, String operador) {
		Operacion operacion = operaciones.get(operador);
		if (operacion == null) {
			System.out.println("No existe la operacion: " + operador);
			return 0;
		}
		return operacion.operar(a, b);
	}
}
