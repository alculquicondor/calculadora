
public class HolaMundo {
	public static void main(String[] args) {
		Calculadora c =  new Calculadora();
		c.registrar("+", new Suma());
		c.registrar("-", new Resta());
        c.registrar("*", new Multiplicacion());
        c.registrar("/", new Division());
        c.registrar("^", new Potenciacion());
		int x = c.ejecutar(2, 5, "^");
		System.out.println("potenciacion 2^5=" + x);
	}
}
